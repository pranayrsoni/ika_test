﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMaster : MonoBehaviour
{

    [SerializeField] private KeyCode changePlayer = KeyCode.Space;

    [SerializeField] private KeyCode player1Forward = KeyCode.W;
    [SerializeField] private KeyCode player1Backward = KeyCode.S;
    [SerializeField] private KeyCode player1Left = KeyCode.A;
    [SerializeField] private KeyCode player1Right = KeyCode.D;

    [SerializeField] private KeyCode player2Forward = KeyCode.UpArrow;
    [SerializeField] private KeyCode player2Backward = KeyCode.DownArrow;
    [SerializeField] private KeyCode player2Left = KeyCode.LeftArrow;
    [SerializeField] private KeyCode player2Right = KeyCode.RightArrow;


    public delegate void Player1Input(Vector2 input);
    public static event Player1Input Player1InputEvent;

    public delegate void Player2Input(Vector2 input);
    public static event Player2Input Player2InputEvent;

    public delegate void OnPlayerChanged();
    public static event OnPlayerChanged OnPlayerChangedEvent;

    private float forwardBackward = 0f;
    private float leftRight = 0f;

    private bool isPlayer1Active = true;

    private Vector2 currentInput;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayer1Active)
        {
            if (Input.GetKey(player1Forward))
            {
                forwardBackward = 1f;
            }
            if (Input.GetKey(player1Backward))
            {
                forwardBackward = -1f;
            }
            if (Input.GetKey(player1Left))
            {
                leftRight = -1f;
            }
            if (Input.GetKey(player1Right))
            {
                leftRight = 1f;
            }

            if (Input.GetKeyUp(player1Forward) || Input.GetKeyUp(player1Backward))
            {
                forwardBackward = 0f;
            }
            if (Input.GetKeyUp(player1Left) || Input.GetKeyUp(player1Right))
            {
                leftRight = 0f;
            }

            currentInput.x = leftRight;
            currentInput.y = forwardBackward;

            if (Player1InputEvent != null)
            {
                Player1InputEvent.Invoke(currentInput);
            }
        }
        else
        {
            if (Input.GetKey(player2Forward))
            {
                forwardBackward = 1f;
            }
            if (Input.GetKey(player2Backward))
            {
                forwardBackward = -1f;
            }
            if (Input.GetKey(player2Left))
            {
                leftRight = -1f;
            }
            if (Input.GetKey(player2Right))
            {
                leftRight = 1f;
            }


            if (Input.GetKeyUp(player2Forward) || Input.GetKeyUp(player2Backward))
            {
                forwardBackward = 0f;
            }
            if (Input.GetKeyUp(player1Left) || Input.GetKeyUp(player2Right))
            {
                leftRight = 0f;
            }

            currentInput.x = leftRight;
            currentInput.y = forwardBackward;

            if (Player2InputEvent != null)
            {

                Player2InputEvent.Invoke(currentInput);
            }
        }

        if (Input.GetKeyUp(changePlayer))
        {
            isPlayer1Active = isPlayer1Active ? false : true;

            forwardBackward = 0f;
            leftRight = 0f;

            if (OnPlayerChangedEvent != null)
            {
                OnPlayerChangedEvent.Invoke();
            }
        }
    }
}
