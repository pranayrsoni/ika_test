﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private bool isPlayer1 = false;
    [SerializeField] private float walkingMultiplier = 10f;
    [SerializeField] private float rotationMultiplier = 50f;
    [SerializeField] private Vegetable vegetableSlot1 = null;
    [SerializeField] private Vegetable vegetableSlot2 = null;
    [SerializeField] private Transform vegetable1Position = null;
    [SerializeField] private Transform vegetable2Position = null;


    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Vegetable>() != null)
        {
            if (vegetableSlot1 == null)
            {
                vegetableSlot1 = other.GetComponent<Vegetable>();
            }
            else if (vegetableSlot1 != null && vegetableSlot2 == null)
            {
                vegetableSlot2 = other.GetComponent<Vegetable>();
            }
            else
            {
                Debug.Log("Cannot carry anymore");
                return;
            }
        }
    }




    private void OnEnable()
    {
        InputMaster.Player1InputEvent += Player1InputEventHandler;
        InputMaster.Player2InputEvent += Player2InputEventHandler;
        InputMaster.OnPlayerChangedEvent += OnPlayerChangedEventHandler;
    }

    private void OnPlayerChangedEventHandler()
    {
        //isPlayer1 = Player1Active;
    }

    private void Player1InputEventHandler(Vector2 input)
    {
        if (!isPlayer1)
            return;

        transform.Translate(0f, 0f, input.y * walkingMultiplier * Time.deltaTime , Space.Self);
        transform.RotateAround(transform.position, Vector3.up, input.x * rotationMultiplier * Time.deltaTime);
    }
    private void Player2InputEventHandler(Vector2 input)
    {
        if (isPlayer1)
            return;

        transform.Translate(input.x * walkingMultiplier * Time.deltaTime, 0f, input.y * walkingMultiplier * Time.deltaTime);
    }

    private void OnDisable()
    {
        InputMaster.Player1InputEvent -= Player1InputEventHandler;
        InputMaster.Player2InputEvent -= Player2InputEventHandler;
        InputMaster.OnPlayerChangedEvent -= OnPlayerChangedEventHandler;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
